package dkeep.logic;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Tests {
	
	Game game;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		game = new Game(1);
	}

	@After
	public void tearDown() throws Exception {
	}
	
	//a) Hero successfully moves into a free cell.
	@Test
	public void a() {
		game.update(Dungeon.Direction.SOUTH);
		assertEquals(1, game.hero.x);
		assertEquals(2, game.hero.y);
		assertEquals(' ', game.dungeon.dungeon[1][1]);
		assertEquals('H', game.dungeon.dungeon[2][1]);
	}
	
	//b) Hero fails to move into a wall.
	@Test
	public void b() {
		game.update(Dungeon.Direction.WEST);
		assertEquals(1, game.hero.x);
		assertEquals(1, game.hero.y);
		assertEquals('H', game.dungeon.dungeon[1][1]);
		assertEquals('X', game.dungeon.dungeon[1][0]);
	}
	
	//c) Hero moves into a cell that contains a sword and wields it.
	@Test
	public void c() {
		for (int i = 0; i < 7; i++) {
			game.update(Dungeon.Direction.SOUTH);
		}
		
		assertTrue(game.hero.hasfoundSword());
		assertEquals('A', game.hero.symbol);
		assertEquals('A', game.dungeon.dungeon[8][1]);
	}
	
	//d) Unarmed hero moves into a cell that is adjacent to a dragon and dies (game ends in defeat).
	@Test
	public void d() {
		for (int i = 0; i < 4; i++) {
			game.update(Dungeon.Direction.SOUTH);
		}
		
		for (int i = 0; i < 2; i++) {
			game.update(Dungeon.Direction.EAST);
		}
		
		assertTrue(game.update(Dungeon.Direction.EAST));
		assertEquals("And thus the hero has failed in his quest. "
				+ "Alas, he has been slain by one of the deadly Dragons ... Game Over", game.getMessage());
	}
	
	//e) Armed hero moves into a cell that is adjacent to a dragon and slays him.
	@Test
	public void e() {
		for (int i = 0; i < 7; i++) {
			game.update(Dungeon.Direction.SOUTH);
		}
		
		for (int i = 0; i < 3; i++) {
			game.update(Dungeon.Direction.NORTH);
		}
		
		for (int i = 0; i < 3; i++) {
			game.update(Dungeon.Direction.EAST);
		}
		
		assertFalse(game.dragons.get(0).isAlive);
		assertEquals(' ', game.dungeon.dungeon[game.dragons.get(0).y][game.dragons.get(0).x]);
	}
	
	//f) Hero moves into the exit cell, after wielding the sword and killing the dragon (game ends in victory).
	@Test
	public void f() {
		for (int i = 0; i < 7; i++) {
			game.update(Dungeon.Direction.SOUTH);
		}
		
		for (int i = 0; i < 3; i++) {
			game.update(Dungeon.Direction.NORTH);
		}
		
		for (int i = 0; i < 5; i++) {
			game.update(Dungeon.Direction.EAST);
		}
		
		for (int i = 0; i < 4; i++) {
			game.update(Dungeon.Direction.NORTH);
		}
		
		for (int i = 0; i < 2; i++) {
			game.update(Dungeon.Direction.EAST);
		}
		
		for (int i = 0; i < 3; i++) {
			game.update(Dungeon.Direction.SOUTH);
		}
		
		assertTrue(game.update(Dungeon.Direction.EAST));
		assertEquals("By bravely slaying every Dragon in the dungeon, "
				+ "the Hero emerges victorious yet again!", game.getMessage());		
	}
	
	//g) Hero fails to move into the exit cell without having wielded the sword.
	@Test
	public void g() {
		for (int i = 0; i < 7; i++) {
			game.update(Dungeon.Direction.EAST);
		}
		
		for (int i = 0; i < 3; i++) {
			game.update(Dungeon.Direction.SOUTH);
		}
		
		assertFalse(game.update(Dungeon.Direction.EAST));
		assertEquals(1 + 7, game.hero.x);
		assertEquals(1 + 3, game.hero.y);
		assertEquals('H', game.dungeon.dungeon[1+3][1+7]);
		assertEquals('E', game.dungeon.dungeon[1+3][1+7+1]);
		assertEquals("The exit door is locked. "
				+ "Slay every Dragon in the dungeon to open it!", game.getMessage());		
	}
	
	//h) Armed hero fails to move into the exit cell without having slain the dragon.
	@Test
	public void h() {
		for (int i = 0; i < 7; i++) {
			game.update(Dungeon.Direction.SOUTH);
		}
		
		for (int i = 0; i < 7; i++) {
			game.update(Dungeon.Direction.NORTH);
		}
		
		for (int i = 0; i < 7; i++) {
			game.update(Dungeon.Direction.EAST);
		}
		
		for (int i = 0; i < 3; i++) {
			game.update(Dungeon.Direction.SOUTH);
		}
		
		assertFalse(game.update(Dungeon.Direction.EAST));
		assertEquals(1 + 7, game.hero.x);
		assertEquals(1 + 3, game.hero.y);
		assertEquals('A', game.dungeon.dungeon[1+3][1+7]);
		assertEquals('E', game.dungeon.dungeon[1+3][1+7+1]);
		assertEquals("The exit door is locked. "
				+ "Slay every Dragon in the dungeon to open it!", game.getMessage());		
	}
	
	//i) Test all four corner dragon movement possibilities.
	@Test
	public void i() {
		game.dragons.get(0).move(Dungeon.Direction.NORTHWEST);
		
		assertEquals(4, game.dragons.get(0).x);
		assertEquals(4, game.dragons.get(0).y);
		assertEquals('D', game.dungeon.dungeon[4][4]);
		assertEquals(' ', game.dungeon.dungeon[5][5]);
		
		game.dragons.get(0).move(Dungeon.Direction.SOUTHWEST);
		
		assertEquals(3, game.dragons.get(0).x);
		assertEquals(5, game.dragons.get(0).y);
		assertEquals('D', game.dungeon.dungeon[5][3]);
		assertEquals(' ', game.dungeon.dungeon[4][4]);
		
		game.dragons.get(0).move(Dungeon.Direction.SOUTHEAST);
		
		assertEquals(4, game.dragons.get(0).x);
		assertEquals(6, game.dragons.get(0).y);
		assertEquals('D', game.dungeon.dungeon[6][4]);
		assertEquals(' ', game.dungeon.dungeon[5][3]);
		
		game.dragons.get(0).move(Dungeon.Direction.NORTHEAST);
	
		assertEquals(5, game.dragons.get(0).x);
		assertEquals(5, game.dragons.get(0).y);
		assertEquals('D', game.dungeon.dungeon[5][5]);
		assertEquals(' ', game.dungeon.dungeon[6][4]);
	}

}
