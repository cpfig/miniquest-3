package dkeep.logic;

public class Hero extends Element {
	
	boolean foundSword = false;
	public boolean foundKey = false;

	boolean hasfoundSword() {
		return foundSword;
	}

	public void setFoundSword(boolean foundSword) {
		this.foundSword = foundSword;
		this.symbol = 'A';
		this.dungeon.placeAtCell(x, y, symbol);
	}
	
	public boolean hasFoundKey() {
		return foundKey;
	}

	public void setFoundKey(boolean foundKey) {
		this.foundKey = foundKey;
	}

	public Hero (int x, int y, Dungeon dungeon) {
		super(x, y, 'H', dungeon);
	}
	
	public void move (Dungeon.Direction dir) {
		
		int deltaX = 0;
		int deltaY = 0;
		
		switch (dir) {
		case NORTH: deltaY = -1;
		break;
		case WEST: deltaX = -1;
		break;
		case SOUTH: deltaY = 1;
		break;
		case EAST: deltaX = 1;
		break;
		default:
		}
		
		if (dungeon.canMoveInto(x + deltaX, y + deltaY)) {
			dungeon.moveElement(symbol, x, deltaX, y, deltaY);
			x += deltaX;
			y += deltaY;
		}
	}
	
}
