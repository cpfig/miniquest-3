package dkeep.logic;

import java.util.Random;

public class Element {
	int x, y;
	char symbol;
	Dungeon dungeon;
	Random rand = new Random();
	
	public Element (int x, int y, char symbol, Dungeon dungeon) {
		this.x = x;
		this.y = y;
		this.symbol = symbol;
		this.dungeon = dungeon;
		
		//dungeon.dungeon[y][x] = symbol;
		dungeon.placeAtCell(x, y, symbol);
	}
	
	//To place the exit at a random but valid location
	public Element (char symbol, Dungeon dungeon) {
		this.symbol = symbol;
		this.dungeon = dungeon;
		
		switch (rand.nextInt(4)) {
		
		case 0: //Upper Wall
			this.y = 0;
			
			do
			{
				this.x = rand.nextInt(dungeon.lengthX - 2) + 1;
			}
			while (!dungeon.placeAtWall(x, 0, symbol, Dungeon.Walls.UPPER));
			break;
			
		case 1: //Left Wall
			this.x = 0;
			
			do
			{
				this.y = rand.nextInt(dungeon.lengthY - 2) + 1;
			}
			while (!dungeon.placeAtWall(0, y, symbol, Dungeon.Walls.LEFT));
			break;
			
		case 2: //Bottom Wall
			this.y = dungeon.lengthY - 1;
			
			do
			{
				this.x = rand.nextInt(dungeon.lengthX - 2) + 1;
			}
			while (!dungeon.placeAtWall(x, dungeon.lengthY - 1, symbol, Dungeon.Walls.BOTTOM));
			break;
			
		case 3: //Right Wall
			this.x = dungeon.lengthX - 1;
			
			do
			{
				this.y = rand.nextInt(dungeon.lengthY - 2) + 1;
			}
			while (!dungeon.placeAtWall(dungeon.lengthX - 1, y, symbol, Dungeon.Walls.RIGHT));
			break;
		}			
	}
	
	public boolean adjacentTo(Element other) {
		
		boolean adjacent = false;
		
		if (this.x + 1 == other.x && this.y == other.y) adjacent = true;
		if (this.x - 1 == other.x && this.y == other.y) adjacent = true;
		if (this.x == other.x && this.y + 1 == other.y) adjacent = true;
		if (this.x == other.x && this.y - 1 == other.y) adjacent = true;
		
		return adjacent;
		
	}
	
	public boolean onTopOf(Element other) {
		return this.x == other.x && this.y == other.y;
	}

}
